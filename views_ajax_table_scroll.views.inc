<?php
/**
 * @file
 * Provides the views plugin information.
 */

/**
 * Implements hook_views_plugin().
 */
function views_ajax_table_scroll_views_plugins() {
  return array(
    'module' => 'views_ajax_table_scroll',
    'pager' => array(
      'ajax_table_scroll' => array(
        'title' => t('Ajax Table Scroll'),
        'help' => t('views_ajax_table_scroll'),
        'handler' => 'views_plugin_pager_ajax_table_scroll',
        'uses options' => TRUE,
        'parent' => 'full',
      ),
    ),
  );
}
